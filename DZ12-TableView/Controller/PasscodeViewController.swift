//
//  PasscodeViewController.swift
//  Pods
//
//  Created by Павел Москалёв on 10.02.21.
//

import LocalAuthentication
import UIKit

class PasscodeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        biometric()

    }

    func biometric () {
        let context = LAContext()
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Please authenticate to proceed") { (sucsess, error) in
                if sucsess {
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    guard let error = error else {return}
                    print(error.localizedDescription)
                }
            }
        }
    }

    

}
