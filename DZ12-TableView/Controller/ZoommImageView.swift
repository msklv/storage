//
//  ZoommImageView.swift
//  TableView
//
//  Created by Павел Москалёв on 11.02.21.
//

import UIKit

class ZoommImageView: UIView {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        scrollView.delegate = self
    }

}

extension ZoommImageView: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }

}
