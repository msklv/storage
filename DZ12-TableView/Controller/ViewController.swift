//
//  ViewController.swift
//  DZ12-TableView
//
//  
//

import UIKit

enum Style {
    case table
    case collection
}

enum Scene {
    case user
    case editor
}

var style : Style = Style.table
var scene : Scene = Scene.user




class ViewController: UIViewController {

    
    //MARK:- Properties -
    
    var folderData: [Folder] = []
    var folderNames: [String] = []
    let fileManager = FileManager.default
    var directoryURL: URL?
    var editsFolder : [Folder] = []
   
    
    
    //MARK:- IBOutlets -
    
    @IBOutlet weak var deleteButton: UIBarButtonItem!
    @IBOutlet weak var ItemNav: UINavigationItem!
    @IBOutlet weak var CollectionView: UICollectionView! {
        didSet {
            CollectionView.delegate = self
            CollectionView.dataSource = self
            CollectionView.register(UINib(nibName: "ImageCell", bundle: nil), forCellWithReuseIdentifier: "ImageCellCV")
            CollectionView.register(UINib(nibName: "FolderCell", bundle: nil), forCellWithReuseIdentifier: "FolderCellCV")
            
        }
    }
    @IBOutlet weak var mainTableView: UITableView! {
        didSet {
            mainTableView.delegate = self
            mainTableView.dataSource = self
            let nib = UINib(nibName: TableViewRegister.nibNameMainTableView.rawValue, bundle: nil)
            let newNib = UINib(nibName: TableViewRegister.nibNameimageTableView.rawValue, bundle: nil)
            mainTableView.register(nib, forCellReuseIdentifier: TableViewRegister.mainTableViewIdentifier.rawValue)
            mainTableView.register(newNib, forCellReuseIdentifier: TableViewRegister.imageTableViewIdentifier.rawValue)
            mainTableView.rowHeight = 100
            mainTableView.tableFooterView = UIView()
        }
    }
    
    @IBOutlet weak var changeStyle: UIBarButtonItem!
    
    //MARK:- LifeCycle -
    override func viewDidLoad() {
        
        super.viewDidLoad()
        CollectionView.allowsMultipleSelection = true
        switch style {
        case .collection:
            mainTableView.layer.opacity = 0
            mainTableView.isUserInteractionEnabled = false
            CollectionView.layer.opacity = 1
            CollectionView.isUserInteractionEnabled = true
            changeStyle.image = UIImage(named: "collection")
        case .table:
            mainTableView.layer.opacity = 1
            mainTableView.isUserInteractionEnabled = true
            CollectionView.layer.opacity = 0
            CollectionView.isUserInteractionEnabled = false
            changeStyle.image = UIImage(named: "table")
        }
        
        if directoryURL == nil {
            directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        }
        getDocuments(nameOfFolder: nil, folderUrls: directoryURL!)
        print(style)
        notification()
    }
    
    //MARK:- Functions -
    
    func notification() {
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.requestAuthorization(options: [.alert , .badge, .sound]) { (isAuthorized, error) in
            if isAuthorized {
                let content = UNMutableNotificationContent()
                content.badge = 1
                content.sound = UNNotificationSound.default
                content.body = "Не забудь добавить новую фотографию"
                content.title = "Привет!"
                let trigger  = UNTimeIntervalNotificationTrigger(timeInterval: 60, repeats: true)
                let identifier = "notification"
                let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
                notificationCenter.add(request) { (error) in
                    if let error = error {
                        print(error)
                    }
                }
            }
        }
    }
    
    @IBAction func GoEdit(_ sender: UIBarButtonItem) {
        scene = .editor
        deleteButton.isEnabled = true
    }
    
    @IBAction func DeleteSelectedItems(_ sender: UIBarButtonItem) {
        for item in editsFolder {
            if let directoryURL = directoryURL , let itemName = item.name  {
                do {
                    try fileManager.removeItem(at: directoryURL.appendingPathComponent(itemName))
                    
                    folderData.removeAll(where: { $0.name == item.name})
                    
                } catch{
                    print("error")
                }
            }
        }
        mainTableView.reloadData()
        CollectionView.reloadData()
        scene = .user
        editsFolder = []
        deleteButton.isEnabled = false
    }
    
    
    @IBAction func ChangeStyle(_ sender: UIBarButtonItem) {
        if style == .table {
            mainTableView.layer.opacity = 0
            mainTableView.isUserInteractionEnabled = false
            CollectionView.layer.opacity = 1
            CollectionView.isUserInteractionEnabled = true
            style = .collection
            changeStyle.image = UIImage(named: "collection")
            
            print(style)
        } else {
            mainTableView.layer.opacity = 1
            mainTableView.isUserInteractionEnabled = true
            CollectionView.layer.opacity = 0
            CollectionView.isUserInteractionEnabled = false
            style = .table
            changeStyle.image = UIImage(named: "table")
            
            print(style)
        }
        
    }
    
    func addAlert() {
        let alert = UIAlertController(title: "Add new folder", message: "", preferredStyle: .alert)
        alert.addTextField { (userNameField) in
            userNameField.font = .systemFont(ofSize: 17)
            userNameField.placeholder = "Name of folder"
            
            let okAction = UIAlertAction(title: "Ok", style: .default) { (_) in
                guard let textField = userNameField.text else { return }
                if (userNameField.text!.isEmpty)  {
                    self.showNotCorrectAlert()
                    return
                } else {
                    self.createDirectory(nameOfFolder: textField, FoldersUrl: self.directoryURL!)
                    self.getDocuments(nameOfFolder: textField, folderUrls: self.directoryURL!)
                }
            }
            alert.addAction(okAction)
        }
        let cancelACtion = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alert.addAction(cancelACtion)
        present(alert, animated: true, completion: nil)
    }
    
    func showNotCorrectAlert() {
        showAlert(title: "Error", message: "Name of folder empty", actionTitles: ["Try again"], actions: [{action1 in
            self.addAlert()
        }])
    }
    
    func createDirectory(nameOfFolder : String, FoldersUrl: URL) {
        
        let directoryURL = FoldersUrl
        
        do {
            let contentOfDirectory = try fileManager.contentsOfDirectory(at: directoryURL, includingPropertiesForKeys: nil)
            let newFolderUrl = directoryURL.appendingPathComponent(nameOfFolder)
            try fileManager.createDirectory(at: newFolderUrl, withIntermediateDirectories: false, attributes: nil)
            
        } catch {
            print("something went wrong")
        }
    }
    
    
    
    func getDocuments(nameOfFolder: String?, folderUrls: URL) {
        let directoryURL = folderUrls
        do {
            folderData = []
            
            folderNames = try fileManager.contentsOfDirectory(at: directoryURL, includingPropertiesForKeys: nil).map {$0.lastPathComponent}
            for i in 0..<folderNames.count {
                if folderNames[i].contains(".jpeg") || folderNames[i].contains(".png") || folderNames[i].contains(".jpg") {
                    self.folderData.append(Folder(name: folderNames[i], imageNamed: "Image", type: .image))
                } else {
                    self.folderData.append(Folder(name: folderNames[i], imageNamed: "Folder", type: .folder))
                }
                folderData.sort(by: {$0.name! < $1.name!})
                
            }
            CollectionView.reloadData()
            mainTableView.reloadData()
        } catch  {
            print("Something went wrong")
        }
    }
    
    func deleteDocuments(nameOfFolder: String , folderUrl: URL) {
        let directoryURL = folderUrl
        do {
            let contentOfDirectory = try fileManager.contentsOfDirectory(at: directoryURL, includingPropertiesForKeys: nil)
            let newFolderUrl = directoryURL.appendingPathComponent(nameOfFolder)
            try fileManager.removeItem(at: newFolderUrl)
        } catch  {
            print("something wrong")
        }
    }
    
    func createImagePicker() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    func newAlert() {
        showAlert(title: "Select an option", message: nil, actionTitles: ["Создать папку","Добавить Фото"], actions:[{action1 in
            self.addAlert()
        },{action2 in
            self.createImagePicker()
        }])
    }
    
    //MARK:- IBActions-
    @IBAction func buttonPress(_ sender: UIBarButtonItem) {
        newAlert()
    }
}

//MARK:- ImagePickerDelegateExtension -
extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let originalImage = info[.originalImage] as? UIImage,
           let url = info[.imageURL] as? URL {
            if let newUrl = directoryURL {
                let directoryUrl = newUrl
                
                
                do {
                    let newImageUrl = (directoryUrl.appendingPathComponent(url.lastPathComponent))
                    
                    let pngImageData = originalImage.pngData()
                    
                    try pngImageData?.write(to: newImageUrl)
                } catch {
                    print("Something went wrong")
                }
            }
        }
        getDocuments(nameOfFolder: nil, folderUrls: directoryURL!)
        picker.dismiss(animated: true, completion: nil)
    }
}

//MARK:- CollectionExtensions -
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return folderData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let currentData = folderData[indexPath.row]
        switch currentData.type {
        case .image:
            if let imageCell = CollectionView.dequeueReusableCell(withReuseIdentifier: "ImageCellCV", for: indexPath) as? ImageCell,
               let imageName = currentData.name {
                imageCell.ImageCellLabel.text = currentData.name
                if let newUrl = directoryURL {
                    do {
                        let imageUrl = newUrl.appendingPathComponent(imageName)
                        imageCell.ImageCellImage.image = UIImage(contentsOfFile: imageUrl.path)
                        
                    }
                    
                    return imageCell
                }
            }
        case .folder:
            if let folderCell = CollectionView.dequeueReusableCell(withReuseIdentifier: "FolderCellCV", for: indexPath) as? FolderCell {
                folderCell.configure(result: folderData[indexPath.row])
                return folderCell
            }
        }
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 130, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentData = folderData[indexPath.item]
        switch scene {
        case .editor:
            editsFolder.append(currentData)
            
        case .user :
            switch currentData.type {
            case .image:
                if let imageName = currentData.name {
                    let imageController = ImageViewController(nibName: ImageViewController.identifier, bundle: nil)
                   
                    if let newUrl = directoryURL {
                        do {
                            let imageUrl = newUrl.appendingPathComponent(imageName)
                            style = .collection
                            self.navigationController?.pushViewController(imageController, animated: true)
                            
                        }
                    }
                }
            case .folder:
                let vc = storyboard?.instantiateViewController(identifier: "mainVC") as! ViewController
                let currentData = folderData[indexPath.row]
                if let folderName = currentData.name, let newDirectoryUrl = directoryURL {
                    do {
                        let folderUrl = newDirectoryUrl.appendingPathComponent(folderName)
                        vc.directoryURL = folderUrl
                    }
                }
                style = .collection
                navigationController?.pushViewController(vc, animated: true)
                print(style)
                
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        switch scene {
        case .editor:
            let currentData = folderData[indexPath.row]
            editsFolder.removeAll(where: {$0.name == currentData.name})
            
        case .user :
            break
        }
    }
}

//MARK:- TableViewExtension -
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return folderData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        imageArray = []
        let currentData = folderData[indexPath.row]
        switch currentData.type {
        case .image:
            if let imageCell = tableView.dequeueReusableCell(withIdentifier: TableViewRegister.imageTableViewIdentifier.rawValue, for: indexPath) as? ImageTableViewCell,
               let imageName = currentData.name {
                imageCell.nameOfImageLabel.text = currentData.name
                
                if let newUrl = directoryURL {
                    do {
                        let imageUrl = newUrl.appendingPathComponent(imageName)
                        imageCell.cellImageView.image = UIImage(contentsOfFile: imageUrl.path)
                    }
                    return imageCell
                }
            }
        case .folder:
            if let folderCell = tableView.dequeueReusableCell(withIdentifier: TableViewRegister.mainTableViewIdentifier.rawValue, for: indexPath) as? MainTableViewCell {
                folderCell.configure(result: folderData[indexPath.row])
                return folderCell
                
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentData = folderData[indexPath.row]
        switch scene {
        case .editor:
            editsFolder.append(currentData)
        case .user:
            switch currentData.type {
            case .image:
                if let imageName = currentData.name , let directoryURL = directoryURL {
                    let imageController = ImageViewController(nibName: ImageViewController.identifier, bundle: nil)
                  
                        do {
                            var imageArray : [URL] = []
                            for object in folderData {
                                let objectName = object.name
                                let objectUrl = directoryURL.appendingPathComponent(objectName!)
                                if object.type == .image {
                                    imageArray.append(objectUrl)
                                    if objectName == imageName {
                                        imageController.index = CGFloat(imageArray.endIndex)
                                    }
                                }
                            }
                            imageController.arrayImageURL = imageArray
                            
                        } catch {
                            print(error)
                        }
                        self.navigationController?.pushViewController(imageController, animated: true)
                        }
                
            case .folder:
                
                let vc = storyboard?.instantiateViewController(identifier: "mainVC") as! ViewController
                let currentData = folderData[indexPath.row]
                if let folderName = currentData.name, let newDirectoryUrl = directoryURL {
                    do {
                        let folderUrl = newDirectoryUrl.appendingPathComponent(folderName)
                        vc.directoryURL = folderUrl
                    
                    if folderName.contains("18+") {
//                                let passcodeVC = PasscodeViewController(nibName: "PasscodeViewController", bundle: nil)
//                                passcodeVC.modalPresentationStyle = .overFullScreen
//                        present(passcodeVC, animated: true)
                    }
                    }
                }
                navigationController?.pushViewController(vc, animated: true)
            }
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        switch scene {
        case .editor:
            let currentData = folderData[indexPath.row]
            editsFolder.removeAll(where: {$0.name == currentData.name})
            
        case .user :
            break
        }
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteDocuments(nameOfFolder: folderData[indexPath.row].name ?? " ", folderUrl: directoryURL!)
            folderData.remove(at: indexPath.row)
            tableView.reloadData()
        }
    }

}
    


