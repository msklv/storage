//
//  ImageViewController.swift
//  DZ12-TableView
//
//  
//

import UIKit

class ImageViewController: UIViewController {
    
    var arrayImageURL : [URL] = []
    var index : CGFloat = 0

    @IBOutlet weak var scrollImageView: UIScrollView!
    @IBOutlet weak var stackImageView: UIStackView!
    
    static let identifier = "ImageViewController"
    var fullImage: UIImage?
        
    override func viewDidLoad() {
        super.viewDidLoad()
          addStackView()
        print(arrayImageURL)

    }
    
    override func viewDidLayoutSubviews() {
        super .viewDidLayoutSubviews()
        scrollImageView.contentOffset = CGPoint(x : view.bounds.width * (index - 1 ), y: 0)
    }

    func addStackView () {
        for index in arrayImageURL{
          let  image = UIImage(contentsOfFile: index.path)
            if let customView = Bundle.main.loadNibNamed("ZoommImageView", owner: nil, options: nil)?.first as? ZoommImageView {
                customView.imageView.image = image
                    stackImageView.addArrangedSubview(customView)
                    NSLayoutConstraint.activate([
                        customView.heightAnchor.constraint(equalTo: scrollImageView.heightAnchor),
                        customView.widthAnchor.constraint(equalTo: scrollImageView.widthAnchor)
                    ])
            }
           
            }
        }
    }
 


