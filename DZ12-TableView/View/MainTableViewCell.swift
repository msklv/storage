//
//  MainTableViewCell.swift
//  DZ12-TableView
//
//  
//

import UIKit

class MainTableViewCell: UITableViewCell {

    @IBOutlet weak var rightarrow: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageLabel: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        rightarrow.image = UIImage(systemName: "arrow.right")
            if selected {
                alpha = 0.5
            } else {
                alpha = 1
                }
            }
        // Configure the view for the selected state
    

 
    func configure(result: Folder) {
        nameLabel.text = result.name
        imageLabel.image = UIImage(named: result.imageNamed)
        
    }
    
}
