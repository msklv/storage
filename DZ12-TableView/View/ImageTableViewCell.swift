//
//  ImageTableViewCell.swift
//  DZ12-TableView
//
//  
//

import UIKit

class ImageTableViewCell: UITableViewCell {

    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var nameOfImageLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            alpha = 0.5
        } else {
            alpha = 1
            }
        }
        // Configure the view for the selected state
    
    
}
