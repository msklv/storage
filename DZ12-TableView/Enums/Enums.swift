//
//  Enums.swift
//  DZ12-TableView
//
//

import Foundation


enum TableViewRegister: String {
    case mainTableViewIdentifier = "mainCell"
    case nibNameMainTableView = "MainTableViewCell"
    case imageTableViewIdentifier = "imageCell"
    case nibNameimageTableView = "ImageTableViewCell"
}



