//

import UIKit
import KeychainSwift

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {

        UIApplication.shared.applicationIconBadgeNumber = 0 
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        
        let passcodeVC = PasscodeViewController(nibName: "PasscodeViewController", bundle: nil)
        passcodeVC.modalPresentationStyle = .overFullScreen
        self.window?.rootViewController?.present(passcodeVC, animated: true, completion: nil)
        
////        
//        
//        let key = KeychainSwift().get("keys")

//        var localVar = UserDefaults.standard.string(forKey: "nameBest")
//        var password = key
//        func firstAlert () {
//            let introAlert = UIAlertController(title: "Рады видеть вас", message: "Укажите пароль, последующие входы будут через ввод данного пароля", preferredStyle: .alert)
//            introAlert.addTextField { (textField) in
//                textField.placeholder = "Password"
//                textField.isSecureTextEntry = true
//                textField.keyboardType = .numberPad
//            }
//            let action = UIAlertAction(title: "Сохранить", style: .default) { (action) in
//                guard  let mas = introAlert.textFields?.first?.text else {return}
//                key.set(mas, forKey: "keys")
//            }
//            introAlert.addAction(action)
//                self.window?.rootViewController?.present(introAlert, animated: true, completion: nil)
//
//        }
//        func secondAlert () {
//            let passwordAlert = UIAlertController(title: "Приветствую", message: "Введите пароль для продолжения", preferredStyle: .alert)
//            passwordAlert.addTextField { (textField) in
//                textField.placeholder = "Password"
//                textField.isSecureTextEntry = true
//                textField.keyboardType = .numberPad
//
//            }
//            let passAction = UIAlertAction(title: "Готово", style: .default) { (_) in
//                let current = passwordAlert.textFields?.first?.text
//                let currentKey = key.get("keys")
//                if current == currentKey {
//
//                } else {
//                    errorAlert ()
//                }
//
//
//            }
//                passwordAlert.addAction(passAction)
//                self.window?.rootViewController?.present(passwordAlert, animated: true, completion: nil)
//        }
//
//        func errorAlert () {
//            let passwordAlert = UIAlertController(title: "Неправильный пароль", message: "Попробуйте ещё раз", preferredStyle: .alert)
//
//            let passAction = UIAlertAction(title: "Повторить", style: .default) { (_) in
//                secondAlert()
//                print(password)
//            }
//                passwordAlert.addAction(passAction)
//                self.window?.rootViewController?.present(passwordAlert, animated: true, completion: nil)
//        }
//        if  keys == nil {
//           firstAlert()
//        } else {
//            secondAlert ()
//        }
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }

    }

