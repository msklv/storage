//
//  FolderCell.swift
//  DZ12-TableView
//
//  Created by Павел Москалёв on 30.01.21.
//

import UIKit

class FolderCell: UICollectionViewCell {

    @IBOutlet weak var FolderCellLabel: UILabel!
    override var isSelected : Bool {
        didSet {
            if isSelected {
                backgroundColor = .blue
            } else {
                backgroundColor = .white
            }
        }
    }
    func configure(result: Folder) {
        FolderCellLabel.text = result.name
     
    }
}
