//
//  ImageCell.swift
//  DZ12-TableView
//
//  Created by Павел Москалёв on 30.01.21.
//

import UIKit

class ImageCell: UICollectionViewCell {

    @IBOutlet weak var ImageCellLabel: UILabel!
    @IBOutlet weak var ImageCellImage: UIImageView!
    
    override var isSelected : Bool {
        didSet {
            if isSelected {
                alpha = 0.3
            } else {
                alpha = 1
            }
        }
//}
}
}
