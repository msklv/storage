//
//  DataModel.swift
//  DZ12-TableView
//
//
//

import Foundation


enum DataType {
    case folder
    case image
}

struct Folder {
    var name: String?
    var imageNamed: String
    var type: DataType
}
